-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2018 at 06:04 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sct_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(5) NOT NULL,
  `faculty_id` int(5) NOT NULL,
  `branch_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `faculty_id`, `branch_name`) VALUES
(1, 1, 'วิทยาการคอมพิวเตอร์'),
(4, 0, 'ระบบสารสนเทศ');

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `building_id` int(20) NOT NULL,
  `building_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`building_id`, `building_name`) VALUES
(5, 'เทคโนโลยีสังคม SC'),
(6, 'เทคโนอุตสาหกรรมการเกษตร');

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `classroom_id` int(50) NOT NULL,
  `equipment_id` int(50) NOT NULL,
  `furniture_t_id` int(50) NOT NULL,
  `furniture_c_id` int(50) NOT NULL,
  `classroom_name` varchar(50) NOT NULL,
  `classroom_num_seat` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`classroom_id`, `equipment_id`, `furniture_t_id`, `furniture_c_id`, `classroom_name`, `classroom_num_seat`) VALUES
(2, 5, 2, 3, 'sc1069', 50),
(3, 6, 1, 3, 'sc1063', 50),
(4, 5, 2, 3, 'sc1061', 50),
(5, 6, 2, 3, 'sc1071', 50),
(7, 6, 2, 3, 'sc1065', 30);

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `equipment_id` int(50) NOT NULL,
  `equipment_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`equipment_id`, `equipment_name`) VALUES
(5, 'แอร์'),
(6, 'ไมค์');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `faculty_id` int(5) NOT NULL,
  `faculty_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`faculty_id`, `faculty_name`) VALUES
(4, 'คณะเทคโนโลยีอุตสาหกรรมการเกษตร'),
(5, 'คณะเทคโนโลยีสังคม');

-- --------------------------------------------------------

--
-- Table structure for table `furniture_c`
--

CREATE TABLE `furniture_c` (
  `furniture_c_id` int(50) NOT NULL,
  `furniture_c_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `furniture_c`
--

INSERT INTO `furniture_c` (`furniture_c_id`, `furniture_c_name`) VALUES
(3, 'เก้าอี้พลาสติกสีน้ำเงิน');

-- --------------------------------------------------------

--
-- Table structure for table `furniture_t`
--

CREATE TABLE `furniture_t` (
  `furniture_t_id` int(50) NOT NULL,
  `furniture_t_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `furniture_t`
--

INSERT INTO `furniture_t` (`furniture_t_id`, `furniture_t_name`) VALUES
(1, 'โต๊ะขาวยาว'),
(2, 'โต๊ะเล็ก');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `member_id` int(5) NOT NULL,
  `branch_id` int(5) NOT NULL,
  `member_username` int(15) NOT NULL,
  `member_password` varchar(50) NOT NULL,
  `member_profile_pic` varchar(255) NOT NULL,
  `member_firstname` varchar(100) NOT NULL,
  `member_lastname` varchar(100) NOT NULL,
  `member_tel` varchar(15) NOT NULL,
  `member_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `testconn_picture_id` int(5) NOT NULL,
  `testconn_picture_name` varchar(50) NOT NULL,
  `testconn_picture_filename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`testconn_picture_id`, `testconn_picture_name`, `testconn_picture_filename`) VALUES
(1, 's', 'e101.png');

-- --------------------------------------------------------

--
-- Table structure for table `request_classroom`
--

CREATE TABLE `request_classroom` (
  `rq_classroom_id` int(5) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `classroom_id` int(50) NOT NULL,
  `rq_day_start` date NOT NULL,
  `rq_day_end` date NOT NULL,
  `rq_time_start` time NOT NULL,
  `rq_time_end` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_classroom`
--

INSERT INTO `request_classroom` (`rq_classroom_id`, `user_id`, `classroom_id`, `rq_day_start`, `rq_day_end`, `rq_time_start`, `rq_time_end`) VALUES
(1, 42, 4, '2018-10-24', '2018-10-24', '09:00:00', '12:00:00'),
(4, 43, 7, '2018-10-25', '2018-10-25', '13:00:00', '17:00:00'),
(5, 42, 5, '2018-10-30', '2018-10-30', '13:00:00', '17:00:00'),
(6, 42, 2, '2018-10-29', '2018-10-29', '09:00:00', '11:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(10) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `m_username` varchar(60) NOT NULL,
  `m_password` varchar(60) NOT NULL,
  `m_type` varchar(20) NOT NULL,
  `firstname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `lastname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `address` varchar(100) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `m_username`, `m_password`, `m_type`, `firstname`, `lastname`, `address`, `image`) VALUES
(41, '', '', '', 'g', 'g', 'g', '1539622045_7432.png'),
(42, 'f', '8fa14cdd754f91cc6554c9e71929cce7', 'admin', 'f', '', '', ''),
(43, 'h', 'e4da3b7fbbce2345d7772b0674a318d5', 'user', 'h', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`),
  ADD KEY `branch_ibfk_1` (`faculty_id`);

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`building_id`);

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`classroom_id`),
  ADD KEY `classroom_ibfk_2` (`furniture_c_id`),
  ADD KEY `classroom_ibfk_3` (`furniture_t_id`),
  ADD KEY `classroom_ibfk_4` (`equipment_id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`equipment_id`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `furniture_c`
--
ALTER TABLE `furniture_c`
  ADD PRIMARY KEY (`furniture_c_id`);

--
-- Indexes for table `furniture_t`
--
ALTER TABLE `furniture_t`
  ADD PRIMARY KEY (`furniture_t_id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`testconn_picture_id`);

--
-- Indexes for table `request_classroom`
--
ALTER TABLE `request_classroom`
  ADD PRIMARY KEY (`rq_classroom_id`),
  ADD KEY `classroom_id` (`classroom_id`),
  ADD KEY `request_classroom_ibfk_1` (`user_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `building_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `classroom`
--
ALTER TABLE `classroom`
  MODIFY `classroom_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `equipment_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `faculty_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `furniture_c`
--
ALTER TABLE `furniture_c`
  MODIFY `furniture_c_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `furniture_t`
--
ALTER TABLE `furniture_t`
  MODIFY `furniture_t_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `member_id` int(5) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `testconn_picture_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `request_classroom`
--
ALTER TABLE `request_classroom`
  MODIFY `rq_classroom_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `classroom`
--
ALTER TABLE `classroom`
  ADD CONSTRAINT `classroom_ibfk_2` FOREIGN KEY (`furniture_c_id`) REFERENCES `furniture_c` (`furniture_c_id`),
  ADD CONSTRAINT `classroom_ibfk_3` FOREIGN KEY (`furniture_t_id`) REFERENCES `furniture_t` (`furniture_t_id`),
  ADD CONSTRAINT `classroom_ibfk_4` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`equipment_id`);

--
-- Constraints for table `member`
--
ALTER TABLE `member`
  ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`);

--
-- Constraints for table `request_classroom`
--
ALTER TABLE `request_classroom`
  ADD CONSTRAINT `request_classroom_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `request_classroom_ibfk_2` FOREIGN KEY (`classroom_id`) REFERENCES `classroom` (`classroom_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
